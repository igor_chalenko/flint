# Flint - Flat Introspection for C++17 and above #

## What is it ##
Flint is a small header-only library that enables tuple-style access to 
the member fields of a C++ object as long as it satisfies certain rules:

* it may not have anonymous union members;
* it may have static fields, but they are skipped;
TODO finish this


### Example ###
```c++
#include <iostream>
#include <string>

#include "flint/flint.h"

struct test_type {
    struct nested1 {
        std::string a;
        struct nested2 { int b; } b;
    } p1;
    int p2{};
    int p3{};
    test_type *p4{};
};

using flint::flatten_fn;

int main() {
    test_type t3{{"x", {42}}, 43, 44, nullptr};

    auto res3 = flatten_fn(t3);
    static_assert(std::is_same_v<
        decltype(res3),
        std::tuple<std::string&, int&, int&, int&, test_type*&>
    >);

    // prints: x
    std::cout << "test_type_3[0] = " << std::get<0>(res3) << std::endl;
    // prints: 42
    std::cout << "test_type_3[1] = " << std::get<1>(res3) << std::endl;
    // prints: 43
    std::cout << "test_type_3[2] = " << std::get<2>(res3) << std::endl;
    // prints: 44
    std::cout << "test_type_3[3] = " << std::get<3>(res3) << std::endl;

    // change t3
    t3.p1.b.b = 101;
    // prints: 101
    std::cout << "new test_type_3[1] = " << std::get<1>(res3) << std::endl;
}
```

Output:
```
test_type_3[0] = x
test_type_3[1] = 42
test_type_3[2] = 43
test_type_3[3] = 44
new test_type_3[1] = 101
```

## Tested platforms ##
 The library is known to work on:
 
 | OS                     | Compiler   | Version  |
 | ---------------------- | ---------- | -------- |
 | Windows 7 and later    | MinGW GCC  | >= 7.2.0 |
 | Ubuntu 18.04 LTS       | GCC        |  9.3.0   |
 | Ubuntu 18.04 LTS       | Clang      |  10.0    |

The library uses some C++17 features and therefore requires a modern compiler 
(as of this writing, "modern" GCC is 9.3.0, and Clang is 10.0).

## Prerequisites ##
* [git](https://git-scm.com/) for getting the package itself and its dependencies;
* [conan](https://conan.io/) for dependency management; see below;
* [cmake](https://cmake.org/) for building; 3.3 or higher is required.
 
## Dependencies  ##
`Flint` depends on `boost.preprocessor` to generate structured bindings of different
sizes. It also uses `googletest` for tests. 
TODO finish this.

### Build ###
Building Flint requires `cmake` 3.3 or higher.
- Go to the project root directory, create a build sub-directory and run
`cmake`:
```
mkdir build && cd build
cmake ../
```
- if the previous step succeeds, run the build:
```
# all the tests must pass
cmake --build --target check .
# if they did, install
cmake --build --target install .
```

## Usage ##
The library consists of the following include files:

- `flint.h` - public API header, defines `flint::flat_get` and `flint::ith_element`;
the remaining headers are only useful for specific purposes
- `append.h` - defines `flint::append`
- `prepend.h` - defines `flint::prepend`
- `decomposable.h` - defines the `decomposable` trait
- `flatten.h` - 
- `to_tuple.h` - defines `flint::to_tuple`
- `tuple_size.h` - defines `flint::tuple_size`

In order to use the library, include the header `flint.h`:
```
// for flint::flat_get and flint::ith_element
#include <flint/flint.h>
```
It in turn includes all the necessary header files (there's no library to
link with). Certain functionality within the library might be useful on its own,
such as `flint::tuple_size` or `flint::to_tuple`. Refer to the corresponding 
bits of documentation to see how to use those without including the main header
in order to reduce compilation time.  

## Documentation ##
The library comes with a short user manual that may be generated using 
the command
```
make apidocs
```
You need either Doxypress installed in order for 
the above command to succeed. There is also a bunch of examples in `example` 
sub-directory, which should help you to get started.
