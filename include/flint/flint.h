// Copyright (c) 2020 Igor Chalenko
// Distributed under the MIT License (MIT).
// See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT

/// @brief Flint - Flat introspection for c++17 and above
/// @details This file is the only public header for the Flint module.

#ifndef FLINT_FLINT_H_
#define FLINT_FLINT_H_

#include "flatten.h"

/**
 * @defgroup group-tuple Tuple functions
 * @defgroup group-introspection Introspection functions
 * @mainpage Flat introspection for c++17 and above
 * @section Motivation
 * First of all, why the whole thing? `magic_get` (link) already does all this and more,
 * right? Yes, with one important difference. Its flat reflection does not (link) support
 * nested non-pod types.
 * @section How does it work?
 *
 */
namespace flint {

/**
 * @ingroup group-introspection
 * Provides compile-time indexed access to the types of the elements of the input object.
 * This is similar to `std::tuple_element`, except that the range of input types is
 * wider. In fact, if `T` is a tuple, the output type is the same as with
 * `std::tuple_element`. Otherwise, `flint::flatten_fn` is used to obtain the result.
 * @tparam I index of the element to locate between 0 and `flint::tuple_size_v<T>`
 * @tparam T input type
 */
template<std::size_t I, typename T>
class ith_element {
    using tuple_type = decltype(flatten_fn(std::declval<T>()));

  public:
    /**
     * The type of Ith element of `T`
     */
    using type = std::tuple_element_t<I, tuple_type>; // std::decay is not needed
};

template<std::size_t I, typename T>
using ith_element_t = typename ith_element<I, T>::type;

template<std::size_t I, typename T>
using ith_element_t = typename ith_element<I, T>::type;

template <std::size_t I, class T>
decltype(auto) flat_get(T && val) noexcept {
    auto tuple = flatten_fn(std::forward<T>(val));
    return std::get<I>(tuple);
}

template <std::size_t I, class T>
decltype(auto) get(T && val) noexcept {
    auto tuple = to_tuple_fn(std::forward<T>(val));
    return std::get<I>(tuple);
}

template<typename T>
class field_count {
    using tuple_type = decltype(to_tuple_fn(std::declval<T>()));
  public:
    static constexpr std::size_t value = std::tuple_size_v<tuple_type>;
};

template<typename T>
class flat_field_count {
    using tuple_type = decltype(flatten_fn(std::declval<T>()));
  public:
    static constexpr std::size_t value = std::tuple_size_v<tuple_type>;
};

template<class T>
constexpr std::size_t field_count_v = field_count<T>::value;

template<class T>
constexpr std::size_t flat_field_count_v = flat_field_count<T>::value;

}
#endif //FLINT_FLINT_H_
