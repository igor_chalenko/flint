// Copyright (c) 2020 Igor Chalenko
// Distributed under the MIT License (MIT).
// See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT

/// @brief Flint - Flat introspection for c++17 and above
/// @details This file contains implementation of the function `to_tuple`.

#ifndef FLINT_TO_TUPLE_H_
#define FLINT_TO_TUPLE_H_

#include <boost/preprocessor/repetition/repeat.hpp>
#include <tuple>

#include "decomposable.h"
#include "tuple_size.h"

namespace flint {

#ifndef TO_TUPLE_MAX
#define TO_TUPLE_MAX 32
#endif

// private namespace
namespace {

template<class T>
constexpr auto to_tuple_impl(T &&, size_constant<0>) noexcept {
    return std::make_tuple();
}

#define TO_TUPLE_P(Z, N, _) , p##N
#define TO_TUPLE_SPECIALIZATION(Z, N, _)                             \
    template<class T>                                                \
    constexpr auto to_tuple_impl(T &&object, size_constant<N + 1>) { \
        auto &[p BOOST_PP_REPEAT_##Z(N, TO_TUPLE_P, nil)] = object;  \
        return std::tie(p BOOST_PP_REPEAT_##Z(N, TO_TUPLE_P, nil));  \
    }
BOOST_PP_REPEAT(TO_TUPLE_MAX, TO_TUPLE_SPECIALIZATION, nil)
#undef TO_TUPLE_SPECIALIZATION
#undef TO_TUPLE_P

template<class T, std::size_t N>
constexpr auto to_tuple_impl(T &&, size_constant<N>) noexcept {
    static_assert(N <= TO_TUPLE_MAX, "Please increase TO_TUPLE_MAX");
}

}// namespace

/**
 * Decomposes an input object and puts every field into a tuple. If the input
 * type is not decomposable, returns the input object unchanged.
 * @see flint::decomposable
 */
struct to_tuple {
    template<typename T>
    auto apply(T &&t) const {
        constexpr static std::size_t SIZE = tuple_size_v<std::decay_t<T>>;
        constexpr bool decomposable = decomposable_v<std::decay_t<T>>;

        if constexpr (decomposable) {
            return to_tuple_impl(std::forward<T>(t), size_constant<SIZE>{});
        } else {
            return std::forward<T>(t);
        }
    }

    template<typename ...Ts>
    auto apply(const std::tuple<Ts...> & t) const {
        return t;
    }
};

template<typename T>
auto to_tuple_fn(T &&t) {
    return to_tuple{}.apply(std::forward<T>(t));
}

}// namespace flint

#endif//FLINT_TO_TUPLE_H_
