// Copyright (c) 2020 Igor Chalenko
// Distributed under the MIT License (MIT).
// See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT

/// @brief Flint - Flat introspection for c++17 and above
/// @details This file contains implementation of the type trait `decomposable`.

#ifndef FLINT_DECOMPOSABLE_H_
#define FLINT_DECOMPOSABLE_H_

#include <string>

namespace flint {

template<typename T>
concept Trait = requires(T a) {
    { T::value } -> std::convertible_to<bool>;
};

/**
 * @section decomposable-definition Definition
 * An object is NOT decomposable, if
 * -# it has a primitive type, a pointer type, a union type, or array type;
 * -# it is of type `std::basic_string`, `std::basic_string_view`, or
 * `std::any`;
 * -# it has an application-defined type that is assignable from a single value
 * (of any type).
 *
 * @section decomposable-discussion Discussion
 * The above definition suggests that decomposition is largely defined by
 * an application, and perhaps by the context within that application. This
 * implies that the actual decomposition trait must be a template parameter in
 * the object-to-tuple conversion code that defaults to `decomposable`.
 * @tparam T an input type
 */
template<typename T>
struct decomposable
    : std::bool_constant<std::is_class_v<T>>{};// and std::is_standard_layout_v<T>> {};

template<typename ...Ts>
struct decomposable<std::tuple<Ts...>> : std::true_type {};

// `std::string` is not decomposable.
template<typename Char, typename Traits, typename Alloc>
struct decomposable<std::basic_string<Char, Traits, Alloc>> : std::false_type {};

/**
 * Helper constant value for `flint::decomposable`.
 * @tparam T
 */
template<typename T>
constexpr bool decomposable_v = decomposable<T>::value;

}// namespace flint

#endif//FLINT_DECOMPOSABLE_H_
