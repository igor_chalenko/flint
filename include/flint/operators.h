//
// Created by crusoe on 31.10.2020.
//

#ifndef FLINT_OPERATORS_H_
#define FLINT_OPERATORS_H_

#include <ostream>

#include "to_tuple.h"

namespace flint {

template<typename S, typename T, typename = void>
struct is_to_stream_writable: std::false_type {};

template<typename S, typename T>
struct is_to_stream_writable<S, T,
                             std::void_t<  decltype( std::declval<S&>()<<std::declval<T>() )  > >
    : std::true_type {};
namespace operators {

namespace detail {
template<typename Tuple, std::size_t... I>
void print(std::ostream &os, const Tuple &tuple, std::index_sequence<I...>) {
    (..., (os << (I == 0 ? "" : ", ") << std::get<I>(tuple)));
}
}

template<typename... Ts>
std::ostream &operator<<(std::ostream &os, const std::tuple<Ts...> &tuple) {
    detail::print(os, tuple, std::make_index_sequence<sizeof...(Ts)>());
    return os;
}

template <typename T, typename = std::enable_if_t<not is_to_stream_writable<std::ostream, T>::value, bool>>
std::ostream& operator<<(std::ostream& os, T const& rhs) {
    constexpr bool streamable = is_to_stream_writable<std::ostream, T>::value;
    if constexpr (not streamable) {
        constexpr bool decomposable = decomposable_v<T>;
        if constexpr (decomposable) {
            return os << "{ " << to_tuple_fn(rhs) << " }";
        }
        else {
            static_assert("need streaming operator for T");
        }
    } else {
        return os << rhs;
    }
}

/*
template <class T>
constexpr bool operator!=(T const& lhs, T const& rhs) {
    return !impl::test_equality(lhs, rhs);
}
*/

}

}

#endif//FLINT_OPERATORS_H_
