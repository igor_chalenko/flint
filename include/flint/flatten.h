// Copyright (c) 2020 Igor Chalenko
// Distributed under the MIT License (MIT).
// See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT

/// @brief Sipapu - C++ logical programming library
/// @details This file contains implementation of the class `flatten`.

#ifndef FLINT_FLATTEN_H_
#define FLINT_FLATTEN_H_

#include "decomposable.h"
#include "prepend.h"
#include "to_tuple.h"

namespace flint {

// private namespace
namespace {

/**
 * @ingroup group-introspection
 * Flattens C++ objects. More specifically, provides a mechanism to recursively apply
 * a function to a sequence of values. Given a value or several values (in the case of
 * tuple), it may replace the first value by some other value, and then recursively
 * process the new value while some condition holds.
 */

/**
 * Implements a single recursion step. First, head of the input sequence is
 * transformed using the input transformation. Then, one of the `helper`
 * specializations is applied to the transformation result.
 * @tparam Transform transformation applied to the sequence of input types
 * @tparam Head the first element in the input sequence
 * @tparam Tail the 2nd, 3rd, and all remaining elements of the input sequence
 */
template<template<typename> typename Decomposable>
struct flatten {
    template<typename Head, typename... Tail>
    auto operator()(Head &&head, Tail &&... tail) {
        constexpr bool decompose = Decomposable<std::decay_t<Head>>::value;
        if constexpr (decompose) {
            return flatten{}(to_tuple_fn(std::forward<Head>(head)), std::forward<Tail>(tail)...);
        } else {
            return prepend(head, flatten{}(std::forward<Tail>(tail)...));
        }
    }

    /**
     * Specialization for tuples.
     * @tparam Transform transformation applied to the sequence of input types
     * @tparam Ts tuple types
     * @tparam Tail types of the rest of the input sequence
     */
    template<typename... Ts, typename... Tail>
    auto operator()(std::tuple<Ts...> &&head, Tail &&... tail) {
        auto action = [&](auto &&... el) {
            return flatten{}(std::forward<Ts>(el)..., std::forward<Tail>(tail)...);
        };
        return std::apply(action, std::forward<std::tuple<Ts...>>(head));
    }

    template<typename... Ts, typename... Tail>
    auto operator()(std::tuple<Ts...> &head, Tail &&... tail) {
        auto action = [&](auto &&... el) {
            return flatten{}(std::forward<Ts>(el)..., std::forward<Tail>(tail)...);
        };
        return std::apply(action, head);
    }

    /**
     * Recursion base. Applied when there are no values left in the input sequence.
     * Creates an empty tuple that will then consume the other values.
     * @tparam Transform transformation applied to the sequence of input types
     */
    auto operator()() {
        return std::tuple<>{};
    }
};

}// namespace

/**
 * @ingroup group-introspection
 * Converts C++ objects into tuples. Given an object of type `T`, which aggregates several
 * objects of types `T1`, `T2`, etc., each of which in turn contains the objects of types
 * `T11`, `T12`, `...`, `T21`, `T22`, `...`, `...`, creates a tuple of type
 * @code
 * std::tuple<T11, T12, ..., T21, T22, ...>
 * @endcode
 * Every type that appears in the resulting tuple is not decomposable. What types are
 * decomposable is defined by the trait `flint::decomposable`. Default implementation
 * assumes that classes with standard layout (as defined by the corresponding traits in
 * namespace `std`) are decomposable. It, however, specifically excludes `std::string`,
 * because implementation of the flattening process requires accessibility of all member
 * fields in the same way as implementation of structured bindings does.
 *
 * @tparam T
 * @param t
 * @return
 */
template<typename T, template<typename> typename Decomposable = decomposable>
requires Trait<Decomposable<T>> auto flatten_fn(T &&t) {
    return flatten<Decomposable>{}(t);
}

}// namespace flint

#endif//FLINT_FLATTEN_H_
