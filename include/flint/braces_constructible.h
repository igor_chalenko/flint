// Copyright (c) 2020 Igor Chalenko
// Distributed under the MIT License (MIT).
// See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT

/// @brief Flint - Flat introspection for c++17 and above
/// @details Borrowed from Boost.DI, this file contains implementation of
/// a trait `is_braces_constructible`, which checks whether a specific type
/// `T` is constructible from arguments `...Args` via expression `T{Args...}`

#ifndef FLINT_BRACES_CONSTRUCTIBLE_H_
#define FLINT_BRACES_CONSTRUCTIBLE_H_

#include <tuple>

namespace flint {
namespace {

// helper alias
template<std::size_t N>
using size_constant = std::integral_constant<std::size_t, N>;

struct wildcard {
    // used by SFINAE substitutions, must be implicit
    template<typename T>
    constexpr operator T() const {}// NOLINT(google-explicit-constructor)
};

template<typename T, std::size_t... I>
[[maybe_unused]]
decltype(void(T{(I, std::declval<wildcard>())...}), std::true_type{})
    braces_constructible_fn(std::index_sequence<I...>);

template<typename, typename...>
std::false_type braces_constructible_fn(...);

/**
 * Checks whether a type `T` is constructible from `N` arguments of unspecified
 * types via expression `T{Args...}`. There are a few implementations of this
 * trait. One is described [here](https://playfulprogramming.blogspot.com/
 * 2016/12/serializing-structs-with-c17-structured.html), for example. It
 * differs in the implementation of `wildcard`, which to the best of my
 * understanding, fails the traits for the types such as:
 * @code
 * struct immobile {
 *   explicit immobile(int x) : a(x) {}
 *   immobile(const immobile&) = delete;
 *
 *   ....
 * };
 * @endcode
 * I didn't dig into this deep enough, but this implementation performs best
 * among the ones I've tried.
 *
 * @tparam T a type under test
 * @tparam N a number of arguments in curly braces for `T`
 */
template<typename T, std::size_t N>
struct is_braces_constructible
    : decltype(braces_constructible_fn<T>(std::make_index_sequence<N>{})) {};

}// namespace
}// namespace flint

#endif//FLINT_BRACES_CONSTRUCTIBLE_H_
