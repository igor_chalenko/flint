// Copyright (c) 2020 Igor Chalenko
// Distributed under the MIT License (MIT).
// See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT

/// @brief Flint - Flat introspection for c++17 and above
/// @details Borrowed from Boost.DI, this code calculates
/// a number of fields in a struct type.

#ifndef FLINT_TUPLE_SIZE_H_
#define FLINT_TUPLE_SIZE_H_

#include "braces_constructible.h"

namespace flint {

namespace {

template<class T, std::size_t L = 0u, std::size_t R = sizeof(T) + 1u>
constexpr std::size_t to_tuple_size_f() {
    constexpr std::size_t M = (L + R) / 2u;
    if constexpr (M == 0) {
        static_assert(std::is_empty_v<T>, "Unable to count fields of `T`");
        return 0u;
    } else if constexpr (L == M) {
        return M;
    } else if constexpr (is_braces_constructible<T, M>::value) {
        return to_tuple_size_f<T, M, R>();
    } else {
        return to_tuple_size_f<T, L, M>();
    }
}

}// namespace

/**
 * @ingroup group-introspection
 * Given a type `T`, provides a compile-time count (`std::integral_constant`) of fields
 * in it. The value equals `1` for primitive types. If there's a nested structure in `T`,
 * it adds `1` to the final count (thus the counting is not recursive).
 * @tparam T input type
 *
 * Example
 * -------
 * @include test/tuple_size.cc
 */
template<class T>
using tuple_size = size_constant<to_tuple_size_f<T>()>;

/**
 * @ingroup group-introspection
 * Helper variable template for `flint::tuple_size`.
 * @tparam T input type
 */
template<class T>
constexpr std::size_t tuple_size_v = tuple_size<T>::value;

}// namespace flint

#endif//FLINT_TUPLE_SIZE_H_
