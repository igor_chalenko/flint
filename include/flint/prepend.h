// Copyright (c) 2020 Igor Chalenko
// Distributed under the MIT License (MIT).
// See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT

/// @file prepend.h This file is a part of `Flint`, flat introspection library
/// for C++. It contains the definition of the function `flint::prepend`.

#ifndef FLINT_PREPEND_H_
#define FLINT_PREPEND_H_

#include <tuple>

namespace flint {

// private namespace
namespace {

template<typename T, typename Tuple, std::size_t... I>
constexpr auto
prepend_impl(T &value, const Tuple &tuple, std::index_sequence<I...>) {
    return std::tie(value, std::get<I>(tuple)...);
}

template<typename T, typename Tuple, std::size_t... I>
constexpr auto
prepend_impl(T &&value, const Tuple &tuple, std::index_sequence<I...>) {
    return std::make_tuple(value, std::get<I>(tuple)...);
}

}// namespace

/**
 * @brief Prepends a given value at the beginning of a tuple.
 * @ingroup group-tuple
 * Given a value of type `T` (as seen in the context of perfect forwarding) and
 * a tuple of type `std::tuple<T1, T2, ...>`, `prepend` creates a new tuple with
 * the input value as a first element, followed by the elements of the input
 * tuple. The function operates in two modes. If the input value is an lvalue
 * reference, the output type is a result of `std::tie`. Otherwise, it's
 * a result of `std::make_tuple`. In other words, the result type is either
 * @code
 *      std::tuple<T&, Ts&...>
 * @endcode
 * or
 * @code
 *     std::tuple<T, Ts...>
 * @endcode
 * This is necessary because the function is used in two different contexts:
 * direct invocation and `decltype` context (with the input arguments created by
 * `std::declval`).
 * @tparam T
 * An element to prepend to the right of the `Tuple`s elements.
 * @tparam Tuple
 * A tuple to expand.
 *
 * @par Example
 * @include test/prepend.cc
 */
template<typename T, typename... Ts>
constexpr auto prepend(T &&value, const std::tuple<Ts...> &tuple) {
    constexpr auto sequence = std::index_sequence_for<Ts...>{};
    return prepend_impl(std::forward<T>(value), tuple, sequence);
}

}// namespace flint

#endif//FLINT_PREPEND_H_
