// Copyright (c) 2020 Igor Chalenko
// Distributed under the MIT License (MIT).
// See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT

/// @brief Flint - Flat introspection for c++17 and above
/// @details Unit tests for the type trait `decomposable`

#include <gtest/gtest.h>

#include "flint/decomposable.h"

using flint::decomposable_v;

struct test_struct {};

static_assert(not decomposable_v<std::string>);
static_assert(not decomposable_v<int>);
static_assert(not decomposable_v<bool>);
static_assert(decomposable_v<test_struct>);

TEST(decomposable, static_assertions) {// NOLINT
}
