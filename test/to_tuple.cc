// Copyright (c) 2020 Igor Chalenko
// Distributed under the MIT License (MIT).
// See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT

/// @brief Flint - Flat introspection for c++17 and above
/// @details Unit tests for `flint::to_tuple_fn`.

#include "flint/to_tuple.h"
#include <gtest/gtest.h>

using flint::to_tuple_fn;

struct test_type {};

struct test_type_2 {
    int a;
    test_type b;
    std::string c;
};

TEST(to_tuple, convert) {// NOLINT
    using tuplified_test_type = decltype(to_tuple_fn(test_type{}));
    static_assert(std::is_same_v<tuplified_test_type, std::tuple<>>);

    test_type_2 t2{1, {}, "x"};
    ASSERT_EQ(t2.c, "x");

    std::cout << flint::decomposable_v<test_type_2> << std::endl;
    auto res2 = to_tuple_fn(t2);
    static_assert(std::is_same_v<
                  decltype(res2),
                  std::tuple<int &, test_type &, std::string &>>);
    ASSERT_EQ(std::get<0>(res2), 1);
    ASSERT_EQ(std::get<2>(res2), "x");
}
