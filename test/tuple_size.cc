// Copyright (c) 2020 Igor Chalenko
// Distributed under the MIT License (MIT).
// See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT

/// @brief Flint - Flat introspection for c++17 and above
/// @details Unit tests for the function `flint::tuple_size`

#include <gtest/gtest.h>

#include "flint/tuple_size.h"

using flint::tuple_size_v;

struct test_struct {};

struct test_struct_2 {
    int a{};
    std::string b;
};

struct test_struct_3 {
    const test_struct_2 &c;

    int a;
    std::string b;
};

struct immobile {
    explicit immobile(int x) : a(x) {}
    immobile(const immobile &) = delete;

    int a;
};

TEST(tuple_size, empty) {// NOLINT
    static_assert(tuple_size_v<test_struct> == 0);
}

TEST(tuple_size, not_empty) {// NOLINT
    test_struct_2 st2{1, "x"};
    test_struct_3 st3{st2, 42, "meaning"};

    ASSERT_EQ(st3.c.a, st2.a);

    static_assert(tuple_size_v<test_struct_2> == 2);
    static_assert(tuple_size_v<test_struct_3> == 3);
    static_assert(tuple_size_v<immobile> == 1);
}