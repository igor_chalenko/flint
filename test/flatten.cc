// Copyright (c) 2020 Igor Chalenko
// Distributed under the MIT License (MIT).
// See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT

/// @brief Flint - Flat introspection for c++17 and above
/// @details Unit tests for `flint::flatten`.

#include <gtest/gtest.h>

#include "flint/flatten.h"
#include "flint/to_tuple.h"

struct test_type {
    int a;
    short b;
};

struct test_type_2 {
    int a;
    std::string b;
};

struct test_type_3 {
    struct nested1 {
        std::string a;
        struct nested2 { int b; } b;
    } p1;
    int p2{};
    int p3{};
    test_type *p4{};
};

struct test_type_4 {
    int p1{};
    std::tuple<std::string, int> p2;
    int p3{};
};

using flint::flatten_fn;

TEST(flatten, flatten) { // NOLINT
    test_type t1{42, 43};
    test_type_2 t2{42, "x"};
    test_type_3 t3{{"x", {42}}, 43, 44, nullptr};

    auto res = flatten_fn(t1);
    static_assert(std::is_same_v<decltype(res), std::tuple<int&, short&>>);
    ASSERT_EQ(std::get<0>(res), t1.a);

    auto res2 = flatten_fn(t2);
    static_assert(std::is_same_v<decltype(res2), std::tuple<int&, std::string&>>);
    ASSERT_EQ(std::get<0>(res2), t2.a);

    auto res3 = flatten_fn(t3);
    static_assert(std::is_same_v<
        decltype(res3),
        std::tuple<std::string&, int&, int&, int&, test_type*&>
    >);

    ASSERT_EQ(std::get<0>(res3), t3.p1.a);
    ASSERT_EQ(std::get<1>(res3), t3.p1.b.b);
    ASSERT_EQ(std::get<2>(res3), t3.p2);
    ASSERT_EQ(std::get<3>(res3), t3.p3);
    ASSERT_EQ(std::get<4>(res3), t3.p4);

    // change t3
    t3.p1.b.b = 101;
    ASSERT_EQ(std::get<1>(res3), 101);

    test_type_4 t4{1, std::make_tuple("x", 1), 2};
    auto res4 = flatten_fn(t4);
    static_assert(std::is_same_v<decltype(res4), std::tuple<int&, std::string&, int&, int&>>);
    ASSERT_EQ(std::get<0>(res4), t4.p1);
    ASSERT_EQ(std::get<1>(res4), std::get<0>(t4.p2));
    ASSERT_EQ(std::get<2>(res4), std::get<1>(t4.p2));
    ASSERT_EQ(std::get<3>(res4), t4.p3);
}
