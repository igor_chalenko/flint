#include <gtest/gtest.h>

#include "flint/tuple_size.h"

struct immobile {
    explicit immobile(int x) : a(x) {}
    immobile(const immobile &) = delete;

    int a;
};

TEST(braces_constructible, immobile) {// NOLINT
    static_assert(flint::is_braces_constructible<immobile, 1>::value);
}
