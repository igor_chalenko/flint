// Copyright (c) 2020 Igor Chalenko
// Distributed under the MIT License (MIT).
// See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT

/// @brief Flint - Flat introspection for c++17 and above
/// @details Unit tests for `flint::flat_get`.

#include <gtest/gtest.h>
#include "flint/flint.h"

using flint::get;
using flint::flat_get;
using flint::ith_element_t;

struct type1 {
    int a;
};

struct type2 {
    std::string a;
    type1 b;
};

TEST(flat_get, tuple) { // NOLINT
    auto res = flat_get<0>(std::make_tuple(42, true));
    static_assert(std::is_same_v<decltype(res), int>);
    ASSERT_EQ(res, 42);
}

TEST(flat_get, type1) { // NOLINT
    auto res = flat_get<0>(type1{42});
    static_assert(std::is_same_v<decltype(res), int>);
    static_assert(std::is_same_v<int&, ith_element_t<0, type1>>);
    ASSERT_EQ(res, 42);
}

TEST(flat_get, type2) { // NOLINT
    auto res = flat_get<0>(type2{"x", 43});
    static_assert(std::is_same_v<decltype(res), std::string>);
    static_assert(std::is_same_v<std::string&, ith_element_t<0, type2>>);
    ASSERT_EQ(res, "x");
    auto res2 = flat_get<1>(type2{"x", 43});
    static_assert(std::is_same_v<decltype(res2), int>);
    static_assert(std::is_same_v<int&, ith_element_t<1, type2>>);
    ASSERT_EQ(res2, 43);

    auto res3 = get<1>(type2{"x", 43});
    static_assert(std::is_same_v<decltype(res3), type1>);
    ASSERT_EQ(res3.a, 43);
}