// Copyright (c) 2020 Igor Chalenko
// Distributed under the MIT License (MIT).
// See accompanying file LICENSE.txt or copy at
// https://opensource.org/licenses/MIT

/// @brief decomposable
/// @details Unit test for `flint::prepend`.

#include <gtest/gtest.h>
#include "flint/prepend.h"

using flint::prepend;

TEST(prepend, to_empty) { // NOLINT
    std::tuple<> tuple1{};
    int a = 2;
    auto res1 = prepend(a, tuple1);
    static_assert(std::is_same_v<decltype(res1), std::tuple<int&>>);
    ASSERT_EQ(std::get<0>(res1), 2);
}

TEST(prepend, to_tuple) { // NOLINT
    int a = 42;
    auto tuple1 = std::make_tuple(1, 2);
    auto res = prepend(a, tuple1);
    static_assert(std::is_same_v<
        decltype(res),
        std::tuple<int&, const int&, const int&>
    >);
    ASSERT_EQ(std::get<0>(res), 42);
    auto res2 = prepend(42, tuple1);
    static_assert(std::is_same_v<decltype(res2), std::tuple<int, int, int>>);
    ASSERT_EQ(std::get<0>(res2), 42);
}

TEST(prepend, rvalue_ref) { // NOLINT
    constexpr auto t = prepend(42, std::make_tuple(1, 2));
    static_assert(std::is_same_v<
        decltype(t),
        const std::tuple<int, int, int>
    >);
    ASSERT_EQ(std::get<0>(t), 42);
}
