//
// Created by crusoe on 31.10.2020.
//
#include <gtest/gtest.h>
#include "flint/operators.h"

struct test_struct_1 {
    int a;
    std::string b;

    auto operator<=>(const test_struct_1&) const = default;
};

struct test_struct_2 {
    test_struct_1 a;
    test_struct_1 b;

    auto operator<=>(const test_struct_2&) const = default;
};

using flint::operators::operator<<;

TEST(operators, equality_comparison) { // NOLINT
    test_struct_1 st1{42, "life"};
    test_struct_2 st2{42, "death", 41, "life"};
    test_struct_1 st3{42, "life"};
    test_struct_2 st4{41, "life", 42, "death"};

    ASSERT_EQ(st1, st3);
    ASSERT_NE(st2, st4);
    ASSERT_LE(st4, st2);

    {
        std::stringstream str{};
        str << st1;
        ASSERT_EQ(str.str(), "{ 42, life }");
    }
    {
        std::stringstream str{};
        str << st2;
        ASSERT_EQ(str.str(), "{ { 42, death }, { 41, life } }");
    }
}