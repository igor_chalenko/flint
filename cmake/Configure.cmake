include(cmake/GetCPM.cmake)
find_package(Threads)

CPMFindPackage(NAME calm-cmake
        GIT_TAG master
        GITHUB_REPOSITORY igor-chalenko/calm-cmake
        GIT_TAG main)

calm_plugins(concepts coverage gtest)
calm_dependency_management(
        GTest:release-1.10.0
        Boost::preprocessor:1.74.0
)
calm_test_dependencies(GTest::gtest GTest::gtest_main Threads::Threads)

