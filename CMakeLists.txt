# Copyright (c) 2020 Igor Chalenko
# Distributed under the Boost Software License, Version 1.0.
# See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt

cmake_minimum_required(VERSION 3.17)
project(flint VERSION 0.0.1 LANGUAGES CXX
        DESCRIPTION "Flat introspection library")

enable_testing()
include(cmake/Configure.cmake)
include(cmake/Build.cmake)
